FROM python:3.13

# renovate: datasource=pypi depName=kopf
ARG KOPF_VERSION=1.37.3

# hadolint ignore=DL3013
RUN pip install --no-cache-dir kopf==${KOPF_VERSION} \
  && pip install --no-cache-dir kubernetes pyyaml requests \
  && adduser --disabled-password --gecos '' --uid 10000 import